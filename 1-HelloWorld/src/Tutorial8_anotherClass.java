import java.util.Scanner;


public class Tutorial8_anotherClass {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Tutorial8_newClass newObject = new Tutorial8_newClass();
		
		System.out.println("Your girlfriend name is: ");
		String temp = input.nextLine();
		newObject.setName(temp);
		newObject.saying();
		System.out.println("\nCalling the function, the name is " + newObject.getName());
	}
}
