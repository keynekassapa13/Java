
public class Tutorial25_final {
	private int sum;
	private final int test;
	
	public Tutorial25_final(int x) {
		test = x;
	}
	
	public void add() {
		sum += test;
	}
	
	public String toString() {
		return String.format("sum = %d \n", sum);
	}
}
