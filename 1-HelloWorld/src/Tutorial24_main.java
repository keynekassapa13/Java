import java.util.EnumSet;

public class Tutorial24_main {
	public static void main(String[] args) {
		for(Tutorial24_enum people: Tutorial24_enum.values()) {
			System.out.printf("%s\t%s\t%s\n", people, people.getDesc(), people.getYear());
		}
		
		System.out.println("\n\ntesting");
		
		for (Tutorial24_enum people: EnumSet.range(Tutorial24_enum.jen, Tutorial24_enum.julia)){
			System.out.printf("%s\t%s\t%s\n", people, people.getDesc(), people.getYear());
		}
	}
}
