
public class Tutorial20_functionParameter {
	public static void main(String[] args) {
		System.out.println(average(1,2,3,4,5,2312,312,31,23,1));
	}
	
	public static int average(int...numbers) {
		int total = 0;
		for (int x: numbers) {
			total += x;
		}
		
		return total/numbers.length;
	}
}
