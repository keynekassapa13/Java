
public class Tutorial25_static {
	private String first;
	private String last;
	private static int member = 0;
	
	public Tutorial25_static(String fn, String ln) {
		first = fn;
		last = ln;
		member++;
		
		System.out.printf("Name is : %s %s, and the members in the group is %d \n", first, last, member);
	}
	
	public String getFirst() {
		return first;
	}
	
	public String getLast() {
		return last;
	}
	
	public static int getMember() {
		return member;
	}
}
