import java.util.*;

public class Solution {
	String alph = "ABCDEFGHIJKLMNOPQRSTUVWZYZ";
	
	public static void main(String[] args) {
		Solution newObject = new Solution();
		String a = newObject.Solution_imc(4, "1B 2C,2D 4D", "2B 2D 3D 4D 4A");
		String b = newObject.Solution_imc(4, "1B 1B,2D 4D", "2B 2D 3D 4D 4A");
	}
	
	public String Solution_imc(int N, String S, String T) {
        // write your code in Java SE 8
        
		String[] shipPos = S.split(",");
        String[][] finalPos = new String[2][5];
        
        System.out.printf("Ship %s, Hit %s \n", S, T);
        
        int sunk = 0;
        int hit = 0;
        
        for (int x=0; x<shipPos.length; x++) {
        	
        	String[] eachPos = shipPos[x].split(" ");
        	String firstPos = eachPos[0];
        	String secondPos = eachPos[1];
        	
        	int counter = 0;
        	String[] temp = new String[5];
        	int diffRow = secondPos.charAt(0) - firstPos.charAt(0);
        	
        	if (diffRow > 0 ) {
        		for (int y = 0; y <= diffRow; y++) {
        			
        			if (secondPos.charAt(1) != firstPos.charAt(1)) {
            			for (int z = 0; z<diffCol(String.valueOf(secondPos.charAt(1)), String.valueOf(firstPos.charAt(1))); z++) {
            				
            				String test = String.valueOf(firstPos.charAt(0));
            				int tempPos = Integer.parseInt(test) + y;
            				
            				String alptest = String.valueOf(firstPos.charAt(1));
            				int intAlp = alph.indexOf(alptest) + z;
            				String tempAlp = String.valueOf(alph.charAt(intAlp));
            				
            				finalPos[x][counter] = String.valueOf(tempPos) + tempAlp;
            				//System.out.println(finalPos[x][counter]);
            				counter++;
            				
            			}
            		} else {
            			String test = String.valueOf(firstPos.charAt(0));
        				int tempPos = Integer.parseInt(test) + y;
        				
        				finalPos[x][counter] = String.valueOf(tempPos) + String.valueOf(firstPos.charAt(1));
        				//System.out.println(finalPos[x][counter]);
            		}
        			
        			counter++;
        		}
        	} else if ( String.valueOf(firstPos).equals(String.valueOf(secondPos))) {
        		finalPos[x][0] = String.valueOf(firstPos);
        	}
        }
        
        for (int x = 0; x<finalPos.length; x++ ) {
            int counter = 0;
            int balance = 0;
        	for (int y = 0; y<finalPos[x].length; y++ ){
        		if (finalPos[x][y]!= null) {
        			balance++;
        		}
        	    for (String eachHit: T.split(" ")){
        	        if (eachHit.equals(finalPos[x][y])){
        	            counter++;
        	        }
        	    }
        	}
        	if (counter == balance){
        	    sunk++;
        	} else if (counter > 0){
        	    hit++;
        	}
        }
        
        System.out.printf("The sunk, hit: %d,%d\n\n", sunk, hit);
        return String.format("%d,%d", sunk, hit);
    }
	
	public int diffCol(String from, String to) {
		int fr = alph.indexOf(from);
		int t = alph.indexOf(to);
		return Math.abs(fr-t+1);
	}
}
