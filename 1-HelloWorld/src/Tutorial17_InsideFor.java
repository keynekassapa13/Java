
public class Tutorial17_InsideFor {
	public static void main(String[] args) {
		int bucky[] = {1,2,3,4,5};
		int sum = 0;
		
		for (int x: bucky) {
			sum += x;
		}
		
		System.out.println(sum);
	}
}
