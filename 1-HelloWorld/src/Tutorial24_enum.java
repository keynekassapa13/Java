
public enum Tutorial24_enum {
	bucky("nice",22),
	jen("cute",19),
	julia("bigmistake",26);
	
	private final String desc;
	private final int year;
	
	Tutorial24_enum(String description, int birthday){
		desc = description;
		year = birthday;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public int getYear() {
		return year;
	}
}
