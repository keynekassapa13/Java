
public class Tutorial19_multiArray {
	public static void main(String[] args) {
		int firstArray[][] = {{1,2,3},{4,5}};
		int secondArray[][] = {{10,11,12},{98,87}};
		
		display(firstArray);
		display(secondArray);
		
	}
	
	public static void display(int x[][]) {
		for (int row=0; row<x.length; row++) {
			for (int col=0; col<x[row].length; col++) {
				System.out.print(x[row][col]+"\t");
			}
			System.out.println("");
		}
	}
}
