
public class Tutorial23_mianFunc {
	private String name;
	private Tutorial23_ConstructorString birthday;
	
	public Tutorial23_mianFunc (String n, Tutorial23_ConstructorString b) {
		name = n;
		birthday = b;
	}
	
	public static void main(String[] args) {
		
	}
	
	public String toString() {
		return String.format("My name is %s, and my birthday is on %s", name, birthday);
	}
}
