import java.util.Scanner;

public class Tutorial9_WhileInput {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int total = 0;
		int counter = 0;
		
		while (counter < 5) {
			total += input.nextInt();
			counter++;
		}
		
		System.out.println(total);
	}
}
