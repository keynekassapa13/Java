
public class Tutorial15_Array {
	public static void main(String[] args) {
		int bucky[] = new int[10];
		
		bucky[0] = 55;
		bucky[1] = 22;
		bucky[2] = 33;
		bucky[9] = 234;
		
		int bucky2[] = {1,3,5,7,9,11,13};
		
		System.out.println(bucky[9]);
		System.out.println(bucky2[6]);
		
		System.out.println("\n\nIndex \tValue");
		
		int arrayEx[] = {512,256,128,64,32,16,8,4,2,1};
		
		for (int counter = 0; counter < arrayEx.length; counter ++) {
			System.out.println(counter+1 + "\t" + arrayEx[counter]);
		}
	}
}
