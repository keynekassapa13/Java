
public class Tutorial21_mainFunc {
	public static void main(String[] args) {
		Tutorial21_StringFunc newObject = new Tutorial21_StringFunc();
		System.out.println(newObject.toMilitary());
		
		newObject.setTime(5, 30, 25);
		System.out.println(newObject.toMilitary());
		System.out.println(newObject.toString());
		
		newObject.setTime(16, 30, 25);
		System.out.println(newObject.toMilitary());
		System.out.println(newObject.toString());
		
		newObject.hour = 10;
		newObject.minute = 10;
		newObject.second = 10;
		System.out.println(newObject.toMilitary());
		System.out.println(newObject.toString());
	}
}
