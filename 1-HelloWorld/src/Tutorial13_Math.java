
public class Tutorial13_Math {
	public static void main(String[] args) {
		System.out.println(Math.abs(-7.6));
		System.out.println(Math.ceil(-7.6));
		System.out.println(Math.floor(-7.6));
		System.out.println(Math.max(-7.6, 6));
		System.out.println(Math.min(-7.6, 6));
		System.out.println(Math.pow(2, 5));
		System.out.println(Math.sqrt(169));
	}
}
