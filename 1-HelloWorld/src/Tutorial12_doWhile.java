
public class Tutorial12_doWhile {
	public static void main(String[] args) {
		int counter = 1;
		
		do {
			System.out.println(counter);
			counter ++;
		} while (counter < 11);
		
	}
}
